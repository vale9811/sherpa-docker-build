IMAGE-NAME="registry.gitlab.com/vale9811/sherpa-docker-build"
SLIM-IMAGE-NAME="registry.gitlab.com/vale9811/sherpa-docker-build/slim"

FULL_DOCKERFILE="./full/Dockerfile"
SLIM_DOCKERFILE="./slim/Dockerfile"

build: build-slim
	docker build -t $(IMAGE-NAME) full

build-slim:
	docker build -t $(SLIM-IMAGE-NAME) slim

build-no-cache: build-slime-no-cache
	docker build --no-cache -t $(IMAGE-NAME) full

build-slim-no-cache:
	docker build --no-cache -t $(SLIM-IMAGE-NAME) slim

.PHONY: push-full
push-full: build
	docker push $(IMAGE-NAME)

.PHONY: push-slim
push-slim: build-slim
	docker push $(SLIM-IMAGE-NAME)

.PHONY: push
push: push-slim push-full

.PHONY: default run build
