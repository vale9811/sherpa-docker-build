
This repo provides two images is based on eachother and `centos:7` and
just installs the build dependencies for `sherpa` to facilitate faster
CI/CD builds.


 - `registry.gitlab.com/vale9811/sherpa-docker-build/slim` includes the build dependencies to compile sherpa
 - `registry.gitlab.com/vale9811/sherpa-docker-build` bases on the
   first image and includes the dependencies to build the manual

## Usage
 - to build the image run: `make`
 - to push the image:
   - authenticate with  `docker login registry.gitlab.com`
   - run `make push`
